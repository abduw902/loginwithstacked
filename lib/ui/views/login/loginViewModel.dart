import 'package:login_with_stacked/app/app.locator.dart';
import 'package:login_with_stacked/app/app.router.dart';
import 'package:login_with_stacked/models/userModel.dart';
import 'package:login_with_stacked/services/api.dart';
import 'package:login_with_stacked/ui/base/authenticationViewModel.dart';
import 'loginView.form.dart';

class LoginViewModel extends AuthenticationViewModel {
  final _apiService = locator<ApiService>();

  LoginViewModel() : super(successRoute: Routes.addressSelectionView);

  @override
  Future<ApiAuthenticationResult> runAuthentication() {
    _apiService.login(email: emailValue, password: passwordValue);
  }

  void navigateToRegistrationPage() =>
      navigationService.navigateTo(Routes.registrationView);
}
