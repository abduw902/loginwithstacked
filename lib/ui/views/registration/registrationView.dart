import 'package:flutter/material.dart';
import 'package:login_with_stacked/ui/dumbWidgets/authenticationLayout.dart';
import 'package:login_with_stacked/ui/views/registration/registrationView.form.dart';
import 'package:login_with_stacked/ui/views/registration/registrationViewModel.dart';
import 'package:stacked/stacked.dart';
import 'package:stacked/stacked_annotations.dart';

@FormView(fields: [
  FormTextField(name: 'email'),
  FormTextField(name: 'password'),
])
class RegistrationView extends StatelessWidget with $RegistrationView{
  RegistrationView({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<RegistrationViewModel>.reactive(
      onModelReady: (model)=>listenToFormUpdated(model),
      builder: (context, model, child) => Scaffold(
        body: AuthenticationLayout(
          busy: model.isBusy,
          onMainButtonTapped: model.saveData,
          onBackPressed: model.navigateBack,
          validationMessage: model.validationMessage,
          title: 'Create Account',
          subtitle: 'Enter your name, email and password for sign up.',
          mainButtonTitle: 'SIGN UP',
          form: Column(
            children: [
              TextField(
                decoration: InputDecoration(labelText: 'Email'),
                controller: emailController,
              ),
              TextField(
                decoration: InputDecoration(labelText: 'Password'),
                controller: passwordController,
              ),
            ],
          ),
          showTermsText: true,
        ),
      ),
      viewModelBuilder: () => RegistrationViewModel(),
    );
  }
}
