import 'package:login_with_stacked/app/app.locator.dart';
import 'package:login_with_stacked/app/app.router.dart';
import 'package:login_with_stacked/services/api.dart';
import 'package:login_with_stacked/ui/base/authenticationViewModel.dart';
import 'registrationView.form.dart';

class RegistrationViewModel extends AuthenticationViewModel {
  final _apiService = locator<ApiService>();
  RegistrationViewModel() : super(successRoute: Routes.addressSelectionView);
  @override
  Future<ApiAuthenticationResult> runAuthentication() {
    _apiService.registration(email: emailValue, password: passwordValue);
  }

  void navigateBack() => navigationService.back();
}
