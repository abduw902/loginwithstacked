import 'package:flutter/foundation.dart';
import 'package:login_with_stacked/app/app.locator.dart';
import 'package:login_with_stacked/services/api.dart';
import 'package:stacked/stacked.dart';
import 'package:stacked_services/stacked_services.dart';

abstract class AuthenticationViewModel extends FormViewModel{
  final navigationService= locator<NavigationService>();
  final String successRoute;
  AuthenticationViewModel({@required this.successRoute});

  @override
  void setFormStatus(){}

  Future saveData() async{
    final result = await runBusyFuture(runAuthentication());
    print(result);

    if(!result.hasError){
      navigationService.replaceWith(successRoute);
    }else{
      setValidationMessage(result.errorMessage);
    }
  }

  Future<ApiAuthenticationResult> runAuthentication();
}