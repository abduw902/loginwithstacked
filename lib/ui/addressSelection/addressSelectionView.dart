import 'package:flutter/material.dart';
import 'package:login_with_stacked/ui/addressSelection/addressSelectionViewModel.dart';
import 'package:stacked/stacked.dart';

class AddressSelectionView extends StatelessWidget {
  AddressSelectionView({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<AddressSelectionViewModel>.reactive(
      builder: (context, model, child) => Scaffold(),
      viewModelBuilder: () => AddressSelectionViewModel(),
    );
  }
}
