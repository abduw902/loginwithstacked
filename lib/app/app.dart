import 'package:login_with_stacked/services/api.dart';
import 'package:login_with_stacked/ui/addressSelection/addressSelectionView.dart';
import 'package:login_with_stacked/ui/views/login/loginView.dart';
import 'package:login_with_stacked/ui/views/registration/registrationView.dart';
import 'package:stacked/stacked_annotations.dart';
import 'package:stacked_services/stacked_services.dart';
@StackedApp(
  routes:[
    //MaterialRoute(page:StartupView),
    CupertinoRoute(page: LoginView, initial: true),
    CupertinoRoute(page: RegistrationView),
    CupertinoRoute(page: AddressSelectionView)
  ],
  dependencies:[
    LazySingleton(classType: NavigationService),
    Singleton(classType: ApiService)
  ],
)
class AppSetup{

}