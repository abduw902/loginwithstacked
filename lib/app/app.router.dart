// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// StackedRouterGenerator
// **************************************************************************

// ignore_for_file: public_member_api_docs

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';

import '../ui/addressSelection/addressSelectionView.dart';
import '../ui/views/login/loginView.dart';
import '../ui/views/registration/registrationView.dart';

class Routes {
  static const String loginView = '/';
  static const String registrationView = '/registration-view';
  static const String addressSelectionView = '/address-selection-view';
  static const all = <String>{
    loginView,
    registrationView,
    addressSelectionView,
  };
}

class StackedRouter extends RouterBase {
  @override
  List<RouteDef> get routes => _routes;
  final _routes = <RouteDef>[
    RouteDef(Routes.loginView, page: LoginView),
    RouteDef(Routes.registrationView, page: RegistrationView),
    RouteDef(Routes.addressSelectionView, page: AddressSelectionView),
  ];
  @override
  Map<Type, StackedRouteFactory> get pagesMap => _pagesMap;
  final _pagesMap = <Type, StackedRouteFactory>{
    LoginView: (data) {
      var args = data.getArgs<LoginViewArguments>(
        orElse: () => LoginViewArguments(),
      );
      return CupertinoPageRoute<dynamic>(
        builder: (context) => LoginView(key: args.key),
        settings: data,
      );
    },
    RegistrationView: (data) {
      var args = data.getArgs<RegistrationViewArguments>(
        orElse: () => RegistrationViewArguments(),
      );
      return CupertinoPageRoute<dynamic>(
        builder: (context) => RegistrationView(key: args.key),
        settings: data,
      );
    },
    AddressSelectionView: (data) {
      var args = data.getArgs<AddressSelectionViewArguments>(
        orElse: () => AddressSelectionViewArguments(),
      );
      return CupertinoPageRoute<dynamic>(
        builder: (context) => AddressSelectionView(key: args.key),
        settings: data,
      );
    },
  };
}

/// ************************************************************************
/// Arguments holder classes
/// *************************************************************************

/// LoginView arguments holder class
class LoginViewArguments {
  final Key key;
  LoginViewArguments({this.key});
}

/// RegistrationView arguments holder class
class RegistrationViewArguments {
  final Key key;
  RegistrationViewArguments({this.key});
}

/// AddressSelectionView arguments holder class
class AddressSelectionViewArguments {
  final Key key;
  AddressSelectionViewArguments({this.key});
}
