import 'dart:convert';

import 'package:login_with_stacked/models/userModel.dart';
import 'package:http/http.dart' as http;

import '../main.dart';

class ApiService {
  Future<ApiAuthenticationResult> login({String email, String password}) async {
    Map data = {
      'email': email,
      'password': password,
    };
    final headers = {'Content-Type': 'application/json'};
    final res = await http.post('$url/api/auth',
        headers: headers, body: jsonEncode(data));
    print(res.statusCode);
    if (res.statusCode == 200 || res.statusCode == 400) {
      print(json.decode(res.body));
      return ApiAuthenticationResult(token: res.body);
    } else {
      return ApiAuthenticationResult.error(errorMessage: "Error");
    }
  }

  Future<ApiAuthenticationResult> registration({String email, String password}) async {
    Map data = {
      'email': email,
      'password': password,
    };
    final headers = {'Content-Type': 'application/json'};
    final res = await http.post('$url/api/register',
        headers: headers, body: jsonEncode(data));
    print(res.statusCode);
    if (res.statusCode == 200) {
      print(res.body);
      return ApiAuthenticationResult.success(successMessage: res.body);
    } else {
      return ApiAuthenticationResult.error(errorMessage: "Error");
    }
  }
}

class ApiAuthenticationResult {
  final String token;
  final String successMessage;
  final String errorMessage;

  ApiAuthenticationResult({this.token}) : errorMessage = null, successMessage=null;
  ApiAuthenticationResult.success({this.successMessage}): errorMessage=null,token=null;
  ApiAuthenticationResult.error({this.errorMessage}) : token = null, successMessage=null;

  bool get hasError => errorMessage != null && errorMessage.isNotEmpty;
}
